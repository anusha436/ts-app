#!/bin/bash

isBase=""

if [ -d /opt/WebSphere/AppServer ];then
       APPPATH=/opt/WebSphere/AppServer/profiles/default/
       isBase=true
elif [ -d /opt/WebSphere/Liberty ];then
       APPPATH=/opt/WebSphere/Liberty/
       isBase=false
fi

if [ -z "$ENVIRONMENT" ]; then

        echo "No targetable customizations to execute ..."
else
        find $APPPATH -type f -name "*.targetable.${ENVIRONMENT}${ENVTYPE}.*" -print0 | while read -d $'\0' f
        do
                new=`echo "$f" | sed -e "s/targetable\.${ENVIRONMENT}${ENVTYPE}\.//"`
                echo "applying taregtable file name on $f to $new"
                mv "$f" "$new"
        done

fi

if [ -n "${ENVTYPE}" ] && [ "${ENVTYPE}" = "live" ] ; then

        run enable-servlet-caching
fi

# Specify was console password
if [[ -n "$adminPassword" ]]
then
  password=$(echo "$adminPassword")
else
  password=`cat /tmp/PASSWORD`
fi

if [[ "$password" != "null" ]] && [[ "$password" != "" ]]; then
        echo "Install Cache Monitor Application"
        /profile/bin/wsadmin.sh -lang jython -conntype NONE -f /SETUP/scripts/cachemonitor.py  -user "configadmin" -password $password
fi

