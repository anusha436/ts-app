<?xml version="1.0" encoding="UTF-8"?>

<!--
 =================================================================
  Licensed Materials - Property of IBM

  WebSphere Commerce

  (C) Copyright IBM Corp. 2007, 2011 All Rights Reserved.

  US Government Users Restricted Rights - Use, duplication or
  disclosure restricted by GSA ADP Schedule Contract with
  IBM Corp.
 =================================================================
-->

<!--
 ===============================================================================
 This Ant file is the default WCBD server deployment script.
 ===============================================================================
-->

<!--
 ===============================================================================
 CUSTOMIZATION: Make a copy of this file as <project>-deploy.xml and follow the
 other CUSTOMIZATION comments in this file.
 ===============================================================================
-->
<project name="wcbd-deploy" default="all">

	<!-- DO NOT change the order of of the following load and import below -->

	<!-- Set up the timestamp for logging -->
	<tstamp>
		<format property="tstamp" pattern="yyyyMMddhhmmss" />
	</tstamp>

	<!-- Load wcbd-build-info.properties to get build info -->
	<property file="${basedir}/wcbd-build-info.properties" />

	<!-- Define WCBD tasks for validation -->
	<path id="init.wcbd.ant.class.path">
		<pathelement location="${basedir}/lib/icu4j.jar" />
		<pathelement location="${basedir}/lib/wcbd-ant.jar" />
		<!--
		 Include the folder that contains the resource bundles for messages
		 printed in the classpath
		-->
		<pathelement location="${basedir}/properties" />
	</path>

	<taskdef name="init.failNL"
	         classname="com.ibm.commerce.wcbd.ant.taskdefs.FailNL"
	         classpathref="init.wcbd.ant.class.path" />
	<taskdef name="init.validateProperty"
	         classname="com.ibm.commerce.wcbd.ant.taskdefs.ValidateProperty"
	         classpathref="init.wcbd.ant.class.path" />

	<!-- Load the required target environment properties file -->
	<init.validateProperty property="target.env" trim="true" nonempty="true" />
	<init.failNL bundle="wcbd-deploy-messages" key="ERR_FILE_NOT_FOUND">
		<arg value="${basedir}/deploy-${target.env}.properties" />
		<condition>
			<and>
				<isfalse value="${delta.mode}" />
				<not>
					<available file="${basedir}/deploy-${target.env}.properties"
					           type="file" />
				</not>
			</and>
		</condition>
	</init.failNL>
	<init.failNL bundle="wcbd-deploy-messages" key="ERR_FILE_NOT_FOUND">
		<arg value="${basedir}/delta-deploy-${target.env}.properties" />
		<condition>
			<and>
				<istrue value="${delta.mode}" />
				<not>
					<available file="${basedir}/delta-deploy-${target.env}.properties"
					           type="file" />
				</not>
			</and>
		</condition>
	</init.failNL>
	<condition property="deploy.properties.file"
	           value="${basedir}/deploy-${target.env}.properties"
	           else="${basedir}/delta-deploy-${target.env}.properties">
		<isfalse value="${delta.mode}" />
	</condition>
	<property file="${deploy.properties.file}" />

	<!--
	 Validate properties that are required for initialization
	-->
	<init.failNL bundle="wcbd-deploy-messages" key="ERR_FILE_NOT_FOUND">
		<arg value="${basedir}/deploy-${target.env}.private.properties" />
		<condition>
			<not>
				<available file="${basedir}/deploy-${target.env}.private.properties"
				           type="file" />
			</not>
		</condition>
	</init.failNL>
	<!--init.validateProperty property="was.home"
	                       trim="true"
	                       nonempty="true"
	                       fileExist="true" /-->
	<init.validateProperty property="wc.home"
	                       trim="true"
	                       nonempty="true"
	                       fileExist="true" />

	<!--
	 ===========================================================================
	 CUSTOMIZATION: Import <project>-deploy-common.xml instead of
	 wcbd-deploy-common.xml if available, so targets are added and overridden as
	 per specification of the Ant <import> task.
	 ===========================================================================
	-->
	<!-- Import the WCBD common deployment Ant file -->
	<import file="${basedir}/wcbd-deploy-common.xml" />

	<!--
	 Encode, decode and load the required target environment private properties
	 file
	-->
	<encodeProperties file="${basedir}/deploy-${target.env}.private.properties" />
	<decodeLoadProperties file="${basedir}/deploy-${target.env}.private.properties" />

	<!-- Create the log and working directory -->
	<mkdir dir="${log.dir}" />
	<mkdir dir="${working.dir}" />

	<!--
	 Runs the server deployment process.
	-->
	<target name="all">
		<trycatch property="throwable.msg" reference="throwable">
			<try>
				<ant antfile="${ant.file}"
				     target="deploy"
				     output="${log.file}" />
				<!-- Send build success e-mail after build tasks are done -->
				<antcall target="mail.success" />
			</try>
			<catch>
				<!-- If build fails, attach log zip file to failure e-mail -->
				<printStackTrace refid="throwable" property="stack.trace" />
				<echo message="${stack.trace}"
				      file="${log.file}"
				      append="true" />
				<delete file="${log.zip.file}" quiet="true" />
				<zip destfile="${log.zip.file}">
					<fileset file="${log.file}" />
				</zip>
				<antcall target="mail.failure">
					<param name="files" value="${log.zip.file}" />
				</antcall>
				<fail message="${throwable.msg}" />
			</catch>
		</trycatch>
	</target>


	<!--
	 Performs the actual deployment tasks.
	-->
	<target name="deploy">
		<!--
		 =======================================================================
		 CUSTOMIZATION: Change the logic of the build process as required below
		 by modifying the <antcall> tasks, calling targets in
		 wcbd-build-common.properties and <project>-deploy-common.properties in
		 the desired sequence.
		 =======================================================================
		-->

		<!-- Set up WCALoggerConfig.xml -->
		<antcall target="setup.wca.logger" />

		<!-- Perform connection tests -->
		<antcall target="test.db" />
		<!--antcall target="test.wsadmin" /-->

		<!-- Perform common dataload -->
		<antcall target="dataload.sql.common" />
		<antcall target="dataload.xml.common" />
		<antcall target="dataload.dataload.common" />
		<antcall target="dataload.acug.common" />
		<antcall target="dataload.acp.common" />
		<antcall target="dataload.acpnls.common" />

		<!-- Perform target-specific dataload -->
		<antcall target="dataload.sql.target" />
		<antcall target="dataload.xml.target" />
		<antcall target="dataload.dataload.target" />
		<antcall target="dataload.acug.target" />
		<antcall target="dataload.acp.target" />
		<antcall target="dataload.acpnls.target" />

		<!-- Prepare Java EE modules for deployment 
		<antcall target="prepare.modules" />-->

		<!-- Create partial app zip for wsadmin deployment 
		<antcall target="build.partial.app" />-->

		<!-- Perform wsadmin deploy 
		<antcall target="wsadmin" />-->

		<!-- Deploy static web assets 
		<antcall target="static.web.deploy" />-->

		<!-- Synchronize WC configuration file
		<antcall target="wc.server.sync" /> -->

		<!-- Deploy data load utility customization 
		<antcall target="dataload.cust.deploy" />-->

		<!-- Clean working directory -->
		<antcall target="clean.working.dir" />
	</target>

</project>
