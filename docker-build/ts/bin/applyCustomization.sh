#!/bin/bash
#-----------------------------------------------------------------
# Licensed Materials - Property of IBM
#
# WebSphere Commerce
#
# (C) Copyright IBM Corp. 2015 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with
# IBM Corp.
#-----------------------------------------------------------------

# Exit this script when an error is encountered
set -e

# This shell scripts can help you to deploy custom package to OOTB transaction docker

cusDir=/SETUP/Cus

component=ts-app

targetPath=/opt/WebSphere/AppServer/profiles/default/installedApps/localhost/ts.ear

# Deperated method updateEJBPackage since 9.0.0.5. Will remove this function after 9.0.0.6
function updateEJBPackage(){
  echo "==========Launch updateEJBPackage==========="
  if [ -d "/SETUP/Cus/Code/$component/ejb" ];then
       cd /SETUP/Cus/Code/$component/ejb
       for module in `ls *.jar`
       do
			compname=` echo $module | cut -d '.' -f1 `
			if [ "$compname" != "" ]; then
				run update-extdata-ejb $compname
			fi
      done	
      cd -
  fi
}

# Deperated method handleCode since 9.0.0.5. Will remove this function after 9.0.0.6
function handleCode(){
  if [ -d "/SETUP/Cus/Code/$component"  ];then
       echo "========== Deploy Custom Code ==========="
        # Iterate all files and folder under /SETUP/Cus/$component/Code to do the copy action
       for item in `ls "/SETUP/Cus/Code/$component"`
       do
            if [ "${item}" != "ejb" ];then
                if [ -d "/SETUP/Cus/Code/$component/${item}" ];then
                   if [ -d "$targetPath/$item" ];then
                        \cp -rfR /SETUP/Cus/Code/$component/$item/* $targetPath/$item
                   else
                        \cp -rfR /SETUP/Cus/Code/$component/$item $targetPath
                   fi
                else 
                    \cp -rfR /SETUP/Cus/Code/$component/$item $targetPath
                fi
            fi
       done

       # Detect if there have EJB project, if yes, will call WAS Admin console command to deploy it
       # Please be aware, this function just will deploy WebSphereCommerceServerExtensionsData.jar
       updateEJBPackage
  fi
}

# Deperated method handleConfig since 9.0.0.5. Will remove this function after 9.0.0.6
function handleConfig(){
  if [ -d "/SETUP/Cus/Config/$component/" ];then
       echo "========== Deploy Custom Configuration ==========="
       mkdir -p /SETUP/app/customization
       \cp -rfR /SETUP/Cus/Config/$component/*  /SETUP/app/customization
  fi
}


function handleCerts(){
  echo "=========== Deploy Custom Certifcation ============"
  if [ -d "/SETUP/Cus/Certs/$component" ];then
        echo "=========== Copy Local Certification ==========="
        mkdir -p /SETUP/certs/custom
        \cp -rfR /SETUP/Cus/Certs/$component/* /SETUP/certs/custom
  fi
}

# Since 9.0.0.5, apply customizaiton logic on TWAS has change to use the was admin partial command to do delta update.
# By this way, it can do complete delta update and support cusotmer to add more customzied EJB and update web.xml to configuration of was node.
function packageDeltaZip(){
  echo "=========== Generate Delta Zip Package ============"
  mkdir -p /SETUP/Cus/delta
  #1. copy files under code folder to /SETUP/Cus/delta
  if [ -d "/SETUP/Cus/Code/$component"  ];then
       # This action just for keep the WCB package folder structure, because we don't want to change customer's behavior
       if [ -d "/SETUP/Cus/Code/$component/ejb" ];then 
          \cp -rfR /SETUP/Cus/Code/$component/ejb/*  /SETUP/Cus/Code/$component
          rm -rf /SETUP/Cus/Code/$component/ejb
       fi 
       \cp -rfR /SETUP/Cus/Code/$component/*  /SETUP/Cus/delta  
  fi

  #2. copy files under config folder to /SETUP/Cus/delta
  if [ -d "/SETUP/Cus/Config/$component/" ];then
          mkdir -p /SETUP/app/customization
          \cp -rfR /SETUP/Cus/Config/$component/*  /SETUP/app/customization
          mkdir -p /SETUP/Cus/delta/xml
          \cp -rfR /SETUP/Cus/Config/$component/*  /SETUP/Cus/delta/xml
  fi 

  #3. zip all files under current folder and name it as ts-app-delta.zip
  cd /SETUP/Cus/delta
  zip -r /SETUP/Cus/ts-app-delta.zip .
  # back to default working dir
  cd /
}

function partialUpdate(){
      packageDeltaZip
      echo "=========== Partial Update TS Ear ============"
      # trigger run engine to do partial Update
      run partial-update-app /SETUP/Cus/ts-app-delta.zip
}

function handleDeployScripts(){
   echo "========== Deploy Custom DeployScripts ==========="
  if [ -d "/SETUP/Cus/DeployScripts/$component" ];then
       if [ -f "/SETUP/Cus/DeployScripts/$component/applyDeployScripts.sh" ];then
            echo "=========== Apply Deploy Scripts ==========="
            /SETUP/Cus/DeployScripts/$component/applyDeployScripts.sh
       fi
  fi
}

function clearDeployPackage(){
   echo "=========== Clear Deploy Pacakge ==========="
   rm -rf /SETUP/Cus
}


## Here defined the folder structure this scripts will handle
#  /SETUP/Cus/
#          |_Code
#          |   |___ts-app  // The relative path of ts.ear
#          |          |___lib
#          |          |___*.jar or *.war
#          |          |___ejb
#          |          |    |___*.jar
#          |          |
#          |          |---properties
#          |          |
#          |          |---xml 
#          |_Config （ optional ）
#          |  |___ts-app  // config file for custom extension. Shell scripts will put them under path /SETUP/app/customization
#          |        
#          |_Certs （ optional ）
#          |   |__ts-app
#          |        |__xx.json // certificaiton content be store in json format. Shell scripts will copy it to /SETUP/certs/custom when container start, certification will load into WAS Key Store and Trust Store
#          |    
#          |_DeployScripts （ optional ）
#              |__ts-app
#                   |
#                   |__applyDeployScripts.sh // customer can define self-owned deploy logic to handle more complex case in Code/Config/Certs folder
#
##

echo "================ Begin Deploy Custom Package ==============="
#1. Detect if /SETUP/Cus/Code and Config exist, if it exist will deploy code
updateEJBPackage
partialUpdate
#2. Detect if /SETUP/Cus/Cert exist, if it exist will deploy Certs
handleCerts
#3. Detect if /SETUP/Cus/DeployScripts exist, if it exist will deploy DeployScripts
handleDeployScripts
#4. Clear customization package
clearDeployPackage
echo "================End deploy customization pacakge================"
