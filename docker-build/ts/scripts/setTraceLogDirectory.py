#-----------------------------------------------------------------
# Licensed Materials - Property of IBM
#
# WebSphere Commerce
#
# (C) Copyright IBM Corp. 2016 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with
# IBM Corp.
#-----------------------------------------------------------------


import sys
sys.path.append('/SETUP/scripts/modules')
from modules import setJvmProperty


# Display usage if input parameters incorrect 
inputSize = len(sys.argv)
if (len(sys.argv) != 1):
	usage = """
Usage:
	set-trace-log-directory <directory>

	Where
		directory  -- The location to write the logs.

	Sets the trace specification.

	"""
        print(usage)
	sys.exit(1)
		
else:

	directory = sys.argv[0]
	
	s1 = AdminConfig.getid('/Server:server1/')

	# Set trace log directory
	file = directory+'/trace.log'
	traceLogId = AdminConfig.showAttribute(AdminConfig.list('TraceService',s1),'traceLog')
	traceLogAttrs = []
	fileName = ['fileName', file]
	maxNumberOfBackupFiles = [ 'maxNumberOfBackupFiles' , '10' ]
	rolloverSize = [ 'rolloverSize' , '100' ]
	traceLogAttrs.append(fileName)
	traceLogAttrs.append(maxNumberOfBackupFiles)
	traceLogAttrs.append(rolloverSize)
	AdminConfig.modify(traceLogId,traceLogAttrs)
	
	# Set activity.log directory
	rasLogId = AdminConfig.showAttribute(AdminConfig.list('RASLoggingService',s1),'serviceLog')
	rasLogAttrs = []
	file = directory+'/activity.log'
	fileName = ['name', file]
	rasLogAttrs.append(fileName)
	AdminConfig.modify(rasLogId, rasLogAttrs)
	
	# Set Http access and error log
	httpLogId = AdminConfig.showAttribute(AdminConfig.list('HTTPAccessLoggingService',s1),'accessLog')
	httperrLogId = AdminConfig.showAttribute(AdminConfig.list('HTTPAccessLoggingService',s1),'errorLog')
	httpLogAttrs = []
	file = directory+'/http_access.log'
	fileName = ['filePath', file]
	httpLogAttrs.append(fileName)
	httperrLogAttrs = []
	file = directory+'/http_error.log'
	fileName = ['filePath', file]
	httperrLogAttrs.append(fileName)
	AdminConfig.modify(httpLogId, httpLogAttrs)
	AdminConfig.modify(httperrLogId, httperrLogAttrs)
	
	
	# Forward ffdc to log directory
	setJvmProperty('com.ibm.ffdc.log', sys.argv[0]+'/ffdc/')
	
	# Set Systemout and System error log directory
	sysFile = directory+'/SystemOut.log'
	errFile = directory+'/SystemErr.log'
	syslog = AdminConfig.showAttribute(s1, 'outputStreamRedirect')
	syserr = AdminConfig.showAttribute(s1, 'errorStreamRedirect')
	logfileName = ['fileName', sysFile]
	errfileName = ['fileName', errFile]
	logAttrs = []
	logAttrs.append(logfileName)
	AdminConfig.modify(syslog, logAttrs)
	
	errAttrs = []
	errAttrs.append(errfileName)
	AdminConfig.modify(syserr, errAttrs)
	
	# Set native_stdout log directory
	naFile = directory+'/native_stdout.log'
	naErrFile = directory+'/native_stderr.log'
	processDef = AdminConfig.list('JavaProcessDef', s1)
	io = AdminConfig.showAttribute(processDef, 'ioRedirect')
	naFileName = ['stdoutFilename', naFile]
	naErrFileName = ['stderrFilename', naErrFile]
	naAttrs = []
	naAttrs.append(naFileName)
	naAttrs.append(naErrFileName)
	
	AdminConfig.modify(io, naAttrs)
	
	print("Log directory is set to " +directory+".")
	AdminConfig.save()
#endIf

